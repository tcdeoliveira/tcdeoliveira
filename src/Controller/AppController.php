<?php
namespace App\Controller;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\I18n;
class AppController extends Controller
{
    public $helpers = [
        'Paginator' => ['templates' => 'Tcdeoliveira.paginator-templates'],
        'Form' => ['templates' => 'Tcdeoliveira.form-templates']
    ];
    /*
        public $paginate = [
        'limit' => 25
        ];
        public $helpers = [
            'Paginator' => ['templates' => 'Tcdeoliveira.paginator-templates'],
            'Form' => ['templates' => 'Tcdeoliveira.form-templates']
        ];
    */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');        
        
    }
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->viewBuilder()->setTheme('Tcdeoliveira');
    }
}