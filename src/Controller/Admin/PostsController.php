<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
class PostsController extends AppController
{
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories', 'Users']
        ];
        $posts = $this->paginate($this->Posts);

        $this->set(compact('posts'));
    }
    public function view($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => ['Categories', 'Users']
        ]);

        $this->set('post', $post);
    }
    public function add()
    {
        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $this->loadcomponent('Tcdeoliveira.UpLoad');
            $this->loadcomponent('Tcdeoliveira.String');
            $post = $this->Posts->patchEntity($post, $this->request->getData());     
            $post->image = !empty($this->request->getData('imagem')) ? $this->UpLoad->send($this->request->getData('imagem'), 'img'.DS.'posts'.DS) : null;
            $post->iname = $this->request->getData('imagem')['name'];
            $post->url = $this->String->generateUrl($post->title);
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            if($post->image!=null){$this->UpLoad->deleteFile($post->image);}
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
        }
        $categories = $this->Posts->Categories->find('list', ['limit' => 200]);
        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        $this->set(compact('post', 'categories', 'users'));
    }
    public function edit($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
        }
        $categories = $this->Posts->Categories->find('list', ['limit' => 200]);
        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        $this->set(compact('post', 'categories', 'users'));
    }
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $post = $this->Posts->get($id);
        if ($this->Posts->delete($post)) {
            $this->Flash->success(__('The post has been deleted.'));
        } else {
            $this->Flash->error(__('The post could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}