<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ProfileJobs Controller
 *
 * @property \App\Model\Table\ProfileJobsTable $ProfileJobs
 *
 * @method \App\Model\Entity\ProfileJob[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProfileJobsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $profileJobs = $this->paginate($this->ProfileJobs);

        $this->set(compact('profileJobs'));
    }

    /**
     * View method
     *
     * @param string|null $id Profile Job id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $profileJob = $this->ProfileJobs->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('profileJob', $profileJob);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $profileJob = $this->ProfileJobs->newEntity();
        if ($this->request->is('post')) {
            $profileJob = $this->ProfileJobs->patchEntity($profileJob, $this->request->getData());
            if ($this->ProfileJobs->save($profileJob)) {
                $this->Flash->success(__('The profile job has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The profile job could not be saved. Please, try again.'));
        }
        $users = $this->ProfileJobs->Users->find('list', ['limit' => 200]);
        $this->set(compact('profileJob', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Profile Job id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $profileJob = $this->ProfileJobs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $profileJob = $this->ProfileJobs->patchEntity($profileJob, $this->request->getData());
            if ($this->ProfileJobs->save($profileJob)) {
                $this->Flash->success(__('The profile job has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The profile job could not be saved. Please, try again.'));
        }
        $users = $this->ProfileJobs->Users->find('list', ['limit' => 200]);
        $this->set(compact('profileJob', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Profile Job id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $profileJob = $this->ProfileJobs->get($id);
        if ($this->ProfileJobs->delete($profileJob)) {
            $this->Flash->success(__('The profile job has been deleted.'));
        } else {
            $this->Flash->error(__('The profile job could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
