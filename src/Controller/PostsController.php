<?php
namespace App\Controller;
use App\Controller\AppController;
class PostsController extends AppController
{   
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories', 'Users']
        ];
        $posts = $this->paginate($this->Posts);
        $this->set(compact('posts'));
    }
}