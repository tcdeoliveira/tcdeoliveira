<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rule $rule
 */
?>
<?php $this->set('title',"Add Rule" ); ?>
<article class='container mh-100'>
    <div class="row rules">
        <div class="col-md-12" style=''>
            <?= $this->Form->create($rule) ?>
                <fieldset>
                    <legend><?= __('Add Rule') ?></legend>
                    <?php
                                                            echo $this->Form->control('name', ['class' => 'form-control']);
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary ']) ?>
                <?= $this->Html->link(__('List Rules') , ['action' => 'index', ], ['class'=>'btn btn-dark ']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</article>