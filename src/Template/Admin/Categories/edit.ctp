<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<?php $this->set('title',"Edit Category" ); ?>
<article class='container mh-100'>
    <div class="row categories">
        <div class="col-md-12" style=''>
            <?= $this->Form->create($category) ?>
                <fieldset>
                    <legend><?= __('Edit Category') ?></legend>
                    <?php
                                                            echo $this->Form->control('name', ['class' => 'form-control']);
                                    echo $this->Input->checkBox('active', ['value' => $category['active']]);
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary ']) ?>
                <?= $this->Html->link(__('List Categories') , ['action' => 'index', ], ['class'=>'btn btn-dark ']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</article>