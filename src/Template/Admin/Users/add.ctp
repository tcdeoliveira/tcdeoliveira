<?php $this->set('title',"Add User" ); ?>
<article class='container mh-100'>
    <div class="row users">
        <div class="col-md-12" style=''>
            <?= $this->Form->create($user) ?>
                <fieldset>
                    <legend><?= __('Add User') ?></legend>
                    <?php
                        echo $this->Form->control('nome', ['class' => 'form-control']);
                        echo $this->Form->control('email', ['class' => 'form-control']);
                        echo $this->Form->control('password', ['class' => 'form-control']);
                        echo $this->Form->control('confirm_password', ['class' => 'form-control', 'type'=>'password']);
                        echo $this->Form->control('rule_id', ['options' => $rules, 'class' => 'form-control', 'label'=>'Regra de Acesso']);
                        echo $this->Input->checkBox('ativo', ['value' => $user['ativo']]);
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary ']) ?>
                <?= $this->Html->link(__('List Users'), ['action' => 'index', ], ['class'=>'btn btn-dark ']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</article>