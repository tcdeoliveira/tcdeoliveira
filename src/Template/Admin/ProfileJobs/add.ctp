<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProfileJob $profileJob
 */
?>
<?php $this->set('title',"Add Profile Job" ); ?>
<article class='container-fluid mh-100'>
    <div class="row profileJobs">
        <div class="col-md-12" style=''>
            <?= $this->Form->create($profileJob) ?>
                <fieldset>
                    <legend><?= __('Add Profile Job') ?></legend>
                    <?php
                        echo $this->Form->control('user_id', ['options' => $users, 'class' => 'form-control']);
                        echo $this->Form->control('empresa', ['class' => 'form-control']);
                        echo $this->Form->control('cargo', ['class' => 'form-control']);
                        echo $this->Form->control('ds', ['class' => 'form-control']);
                        echo $this->Form->control('imagem', ['class' => 'form-control']);
                        echo $this->Input->date('inicio', $profileJob->inicio );
                        echo $this->Input->date('fim', $profileJob->fim );
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary ']) ?>
                <?= $this->Html->link(__('List Profile Jobs') , ['action' => 'index', ], ['class'=>'btn btn-dark ']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</article>