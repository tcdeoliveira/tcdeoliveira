<?php $this->set('title',"View Profile Job" ); ?>
<div class='container-fluid' style=''>
    <div class=" row">
        <div class="col-md-12"  >
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow" style='margin-bottom:15px'>
            <a class="navbar-brand" href="#">    <?= __('Profile Job Actions') ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mr-auto">
                </ul>
                <ul class="my-2 my-lg-0">     
                    <?= $this->Html->link("<i class='material-icons'>format_list_numbered</i>", ['action' => 'index'], ['escape'=>false, 'class'=>'btn btn-primary btn-sm ']) ?>
                    <?= $this->Html->link("<i class='material-icons'>format_paint</i>", ['action' => 'edit',  $profileJob->id], ['escape'=>false,'class'=>'btn btn-warning btn-sm ']) ?>
                    <?= $this->Form->postLink("<i class='material-icons'>delete_forever</i>", ['action' => 'delete',  $profileJob->id], ['escape'=>false,'class'=>'btn btn-danger btn-sm ','confirm' => __('Are you sure you want to delete # {0}?', $profileJob->id)]) ?>
                </ul>
            </div>
            </nav>
        </div>
    </div>
    <div class='row'>    
    <div class="col-md-12" >        
        <h3>#<?= h($profileJob->id) ?></h3>
        <div class="table-responsive ">
        <table class="table table-striped table-hover  table-dark shadow">
                                                                                            <tr>
                            <th scope="row"><?= __('User') ?></th>
                            <td><?= $profileJob->has('user') ? $this->Html->link($profileJob->user->nome, ['controller' => 'Users', 'action' => 'view', $profileJob->user->id]) : '' ?></td>
                        </tr>
                                                                                <tr>
                            <th scope="row"><?= __('Empresa') ?></th>
                            <td><?= h($profileJob->empresa) ?></td>
                        </tr>
                                                                                <tr>
                            <th scope="row"><?= __('Cargo') ?></th>
                            <td><?= h($profileJob->cargo) ?></td>
                        </tr>
                                                                                                            <tr>
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $this->Number->format($profileJob->id) ?></td>
                    </tr>
                                    <tr>
                        <th scope="row"><?= __('Imagem') ?></th>
                        <td><?= $this->Number->format($profileJob->imagem) ?></td>
                    </tr>
                                                                            <tr>
                        <th scope="row"><?= __('Inicio') ?></th>
                        <td><?= h($profileJob->inicio) ?></td>
                    </tr>
                                    <tr>
                        <th scope="row"><?= __('Fim') ?></th>
                        <td><?= h($profileJob->fim) ?></td>
                    </tr>
                                    <tr>
                        <th scope="row"><?= __('Created') ?></th>
                        <td><?= h($profileJob->created) ?></td>
                    </tr>
                                    <tr>
                        <th scope="row"><?= __('Modified') ?></th>
                        <td><?= h($profileJob->modified) ?></td>
                    </tr>
                                                </table>
        </div>
                                    <div class="row">
                    <h4><?= __('Ds') ?></h4>
                    <?= $this->Text->autoParagraph(h($profileJob->ds)); ?>
                </div>
                        </div>    
</div>




    </div>
</div>