<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProfileJob $profileJob
 */
?>
<?php $this->set('title',"Edit Profile Job" ); ?>
<article class='container mh-100'>
    <div class="row profileJobs">
        <div class="col-md-12" style=''>
            <?= $this->Form->create($profileJob) ?>
                <fieldset>
                    <legend><?= __('Edit Profile Job') ?></legend>
                    <?php
                                                            echo $this->Form->control('user_id', ['options' => $users, 'class' => 'form-control']);
                                    echo $this->Form->control('empresa', ['class' => 'form-control']);
                                    echo $this->Form->control('cargo', ['class' => 'form-control']);
                                    echo $this->Form->control('ds', ['class' => 'form-control']);
                                    echo $this->Form->control('imagem', ['class' => 'form-control']);
                                    echo $this->Input->date('inicio', ['valeu' =>  $profileJob['inicio'] ]);
                                    echo $this->Form->control('inicio', ['class' => 'form-control']);
                                    echo $this->Input->date('fim', ['valeu' =>  $profileJob['fim'] ]);
                                    echo $this->Form->control('fim', ['class' => 'form-control']);
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary ']) ?>
                <?= $this->Html->link(__('List Profile Jobs') , ['action' => 'index', ], ['class'=>'btn btn-dark ']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</article>