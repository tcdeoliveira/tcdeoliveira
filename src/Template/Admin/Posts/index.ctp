<?php $this->set('title',"Post" ); ?>
<article class='container-fluid'>
<div class='row'>
<div class="col-md-12">  
            <?= $this->Form->create('search', ['url' => [ 'action' => 'search']]) ?>
                <div class="input-group">            
                    <input type="text" class="form-control" placeholder="find " aria-label="find " aria-describedby="button-addon4">
                    <div class="input-group-append" id="button-addon4">
                        <button class="btn btn-primary" type="submit"><?= __('Find') ?></button>
                        <?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-dark ']) ?>
                    </div>            
                </div>
            <?= $this->Form->end() ?>
            <hr>
        </div> 
</div>
</article>
<article class='container-fluid mh-100'>
        <div class="row" style="">           
        <div class="col-md-12">      
            <div class="table-responsive">
                <table class="table table-striped table-hover  table-dark" id="data">
                    <thead class="thead-dark">
                        <tr>
                            <th ><?= $this->Paginator->sort('id') ?></th>
                            <th ><?= $this->Paginator->sort('category_id') ?></th>
                            <th ><?= $this->Paginator->sort('title') ?></th>
                            <th ><?= $this->Paginator->sort('url') ?></th>
                            <th ><?= $this->Paginator->sort('image') ?></th>
                            <th ><?= $this->Paginator->sort('active') ?></th>
                            <th ><?= $this->Paginator->sort('user_id') ?></th>
                            <th ><?= $this->Paginator->sort('created') ?></th>
                            <th ><?= $this->Paginator->sort('modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($posts as $post): ?>
                            <tr>
                                <td><?= $this->Number->format($post->id) ?></td>
                                <td><?= $post->has('category') ? $this->Html->link($post->category->name, ['controller' => 'Categories', 'action' => 'view', $post->category->id]) : '' ?></td>
                                <td><?= h($post->title) ?></td>
                                <td><?= h($post->url) ?></td>
                                <td><?= h($post->image) ?></td>
                                <td>
                                    <?php 
                                        if($post->active){
                                            echo "
                                            <i class='material-icons text-success'>done</i>
                                            ";
                                        }
                                        else{
                                            echo "
                                            <i class='material-icons text-danger'>done</i>
                                            ";
                                        }
                                    ?>
                                </td>
                                <td><?= $post->has('user') ? $this->Html->link($post->user->nome, ['controller' => 'Users', 'action' => 'view', $post->user->id]) : '' ?></td>
                                <td><?= h($post->created) ?></td>
                                <td><?= h($post->modified) ?></td>
                                <td class="actions"  style="white-space:nowrap">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $post->id], ['class'=>'btn btn-info btn-sm ']) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $post->id], ['class'=>'btn btn-warning btn-sm ']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>       
        </div>
        <div class="paginator col-md-12" style="margin-top:20px">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('First')) ?>
                    <?= $this->Paginator->prev('< ' . __('Back')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Next') . ' >') ?>
                    <?= $this->Paginator->last(__('Last') . ' >>') ?>
                </ul>
            </nav>
            <i class="text-secondary">
                <small>
                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </small>    
            </i>
        </div>
    </div>
</article>