<?php $this->set('title',"Edit Post" ); ?>
<article class='container mh-100'>
    <div class="row posts">
        <div class="col-md-12" style=''>
            <?= $this->Form->create($post, ['type'=>'file']) ?>
                <fieldset>
                    <legend><?= __('Edit Post') ?></legend>
                    <img src='<?= $this->Url->build(DS.$post->image) ?>' >
                    <?php
                        echo $this->Form->control('category_id', ['options' => $categories, 'class' => 'form-control']);
                        echo $this->Input->text('title',['required'=>1,'Error'=>$post->Errors('title'), 'value'=>$post->title]);
                        echo $this->Input->file('imagem', ['value'=>$post->iname,'Errors'=>$post->Errors('imagem')]);
                        echo $this->Input->textarea('text', ['required'=>1,'Errors'=>$post->Errors('text'),'value'=>$post->text]);
                        echo $this->Input->checkBox('active', ['value' => $post->active, 'label'=>'Postagem visível no site / Postagem não visível no site']);                        
                        echo $this->Form->control('user_id', ['options' => $users, 'class' => 'form-control']);
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary ']) ?>
                <?= $this->Html->link(__('List Posts') , ['action' => 'index', ], ['class'=>'btn btn-dark ']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</article>