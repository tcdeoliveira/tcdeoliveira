<?php $this->set('title',"View Post" ); ?>
<div class='container-fluid' style=''>
    <div class=" row">
        <div class="col-md-12"  >
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow" style='margin-bottom:15px'>
            <a class="navbar-brand" href="#">    <?= __('Post Actions') ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mr-auto">
                </ul>
                <ul class="my-2 my-lg-0">     
                    <?= $this->Html->link("<i class='material-icons'>format_list_numbered</i>", ['action' => 'index'], ['escape'=>false, 'class'=>'btn btn-primary btn-sm ']) ?>
                    <?= $this->Html->link("<i class='material-icons'>format_paint</i>", ['action' => 'edit',  $post->id], ['escape'=>false,'class'=>'btn btn-warning btn-sm ']) ?>
                    <?= $this->Form->postLink("<i class='material-icons'>delete_forever</i>", ['action' => 'delete',  $post->id], ['escape'=>false,'class'=>'btn btn-danger btn-sm ','confirm' => __('Are you sure you want to delete # {0}?', $post->title)]) ?>
                </ul>
            </div>
            </nav>
        </div>
    </div>
    <div class='row'>    
    <div class="col-md-12" >        
        <h3>#<?= h($post->title) ?></h3>
        <div class="table-responsive ">
        <table class="table table-striped table-hover  table-dark shadow">
                                                                                            <tr>
                            <th scope="row"><?= __('Category') ?></th>
                            <td><?= $post->has('category') ? $this->Html->link($post->category->name, ['controller' => 'Categories', 'action' => 'view', $post->category->id]) : '' ?></td>
                        </tr>
                                                                                <tr>
                            <th scope="row"><?= __('Title') ?></th>
                            <td><?= h($post->title) ?></td>
                        </tr>
                                                                                <tr>
                            <th scope="row"><?= __('Url') ?></th>
                            <td><?= h($post->url) ?></td>
                        </tr>
                                                                                <tr>
                            <th scope="row"><?= __('Image') ?></th>
                            <td><?= h($post->image) ?></td>
                        </tr>
                                                                                                    <tr>
                            <th scope="row"><?= __('User') ?></th>
                            <td><?= $post->has('user') ? $this->Html->link($post->user->nome, ['controller' => 'Users', 'action' => 'view', $post->user->id]) : '' ?></td>
                        </tr>
                                                                                                            <tr>
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $this->Number->format($post->id) ?></td>
                    </tr>
                                                                            <tr>
                        <th scope="row"><?= __('Created') ?></th>
                        <td><?= h($post->created) ?></td>
                    </tr>
                                    <tr>
                        <th scope="row"><?= __('Modified') ?></th>
                        <td><?= h($post->modified) ?></td>
                    </tr>
                                                                            <tr>
                        <th scope="row"><?= __('Active') ?></th>
                        <td><?= $post->active ? __('Yes') : __('No'); ?></td>
                    </tr>
                                    </table>
        </div>
                                    <div class="row">
                    <h4><?= __('Text') ?></h4>
                    <?= $post->text; ?>
                </div>
                        </div>    
</div>




    </div>
</div>