<?php $this->set('title',"Rule" ); ?>
         
         
<article class='container'>
    <div class='row'>
    <div class="col-md-12">  
            <?= $this->Form->create('search', ['url' => [ 'action' => 'search']]) ?>
                <div class="input-group">            
                    <input type="text" class="form-control" placeholder="find " aria-label="find " aria-describedby="button-addon4">
                    <div class="input-group-append" id="button-addon4">
                        <button class="btn btn-primary" type="submit"><?= __('Find') ?></button>
                        <?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-dark ']) ?>
                    </div>            
                </div>
            <?= $this->Form->end() ?>
            <hr>
        </div>  
    </div>
</article>
<article class='container-fluid mh-100'>
        <div class="row" style="">
        <div class="col-md-12">      
            <div class="table-responsive">
                <table class="table table-striped table-hover  table-dark" id="data">
                    <thead class="thead-dark">
                        <tr>
                                                            <th ><?= $this->Paginator->sort('id') ?></th>
                                                            <th ><?= $this->Paginator->sort('name') ?></th>
                                                        <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($rules as $rule): ?>
                            <tr>
                                                                                                                                                                                                                                    <td><?= $this->Number->format($rule->id) ?></td>
                                                                                                                                                                                                                                                                                                    <td><?= h($rule->name) ?></td>
                                                                                                                                                                <td class="actions"  style="white-space:nowrap">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $rule->id], ['class'=>'btn btn-info btn-sm ']) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rule->id], ['class'=>'btn btn-warning btn-sm ']) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $rule->id], ['class'=>'btn btn-danger btn-sm ','confirm' => __('Are you sure you want to delete # {0}?', $rule->id)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>       
        </div>
        <div class="paginator col-md-12" style="margin-top:20px">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('First')) ?>
                    <?= $this->Paginator->prev('< ' . __('Back')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Next') . ' >') ?>
                    <?= $this->Paginator->last(__('Last') . ' >>') ?>
                </ul>
            </nav>
            <i class="text-secondary">
                <small>
                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </small>    
            </i>
        </div>
    </div>
</article>