<?php $this->set('title',"View Rule" ); ?>
<div class='container-fluid' style=''>
    <div class=" row">
        <div class="col-md-12"  >
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow" style='margin-bottom:15px'>
            <a class="navbar-brand" href="#">    <?= __('Rule Actions') ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mr-auto">
                </ul>
                <ul class="my-2 my-lg-0">     
                    <?= $this->Html->link("<i class='material-icons'>format_list_numbered</i>", ['action' => 'index'], ['escape'=>false, 'class'=>'btn btn-primary btn-sm ']) ?>
                    <?= $this->Html->link("<i class='material-icons'>format_paint</i>", ['action' => 'edit',  $rule->id], ['escape'=>false,'class'=>'btn btn-warning btn-sm ']) ?>
                    <?= $this->Form->postLink("<i class='material-icons'>delete_forever</i>", ['action' => 'delete',  $rule->id], ['escape'=>false,'class'=>'btn btn-danger btn-sm ','confirm' => __('Are you sure you want to delete # {0}?', $rule->name)]) ?>
                </ul>
            </div>
            </nav>
        </div>
    </div>
    <div class='row'>    
    <div class="col-md-12" >        
        <h3>#<?= h($rule->name) ?></h3>
        <div class="table-responsive ">
        <table class="table table-striped table-hover  table-dark shadow">
                                                                        <tr>
                            <th scope="row"><?= __('Name') ?></th>
                            <td><?= h($rule->name) ?></td>
                        </tr>
                                                                                                            <tr>
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $this->Number->format($rule->id) ?></td>
                    </tr>
                                                            </table>
        </div>
            </div>    
</div>
            <div class="related row">
        <div class="col-md-12">
            <h4><?= __('Related Users') ?></h4>
            <?php if (!empty($rule->users)): ?>
                <div class="table-responsive">
                    <table class="table table-hover table-striped" cellpadding="0" cellspacing="0">
                        <tr>
                                                                <th scope="col"><?= __('Id') ?></th>
                                                                <th scope="col"><?= __('Nome') ?></th>
                                                                <th scope="col"><?= __('Email') ?></th>
                                                                <th scope="col"><?= __('Password') ?></th>
                                                                <th scope="col"><?= __('Rule Id') ?></th>
                                                                <th scope="col"><?= __('Ativo') ?></th>
                                                                <th scope="col"><?= __('Created') ?></th>
                                                                <th scope="col"><?= __('Modified') ?></th>
                                                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($rule->users as $users): ?>
                        <tr>
                                                            <td><?= h($users->id) ?></td>
                                                            <td><?= h($users->nome) ?></td>
                                                            <td><?= h($users->email) ?></td>
                                                            <td><?= h($users->password) ?></td>
                                                            <td><?= h($users->rule_id) ?></td>
                                                            <td><?= h($users->ativo) ?></td>
                                                            <td><?= h($users->created) ?></td>
                                                            <td><?= h($users->modified) ?></td>
                                                                                    <td class="actions">
                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                                <?= $this->Html->link(__('Editar'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            <?php endif; ?>
        </div>
    </div>




    </div>
</div>