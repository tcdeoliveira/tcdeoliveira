<script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>
<?php $this->set('title',"Home | tcdeoliveira.com.br" ); ?>
<div class='container'>
    <div class='row'>
        <div class='col-md-9 text-secondary'>            
            <?php foreach($posts as $post): ?>
                <div class='row' style='margin-bottom:0px'>
                    <div class='col-md-3' name='<?= $post->id ?>' id='<?= $post->id ?>'>
                        <img style='margin-bottom:10px' class='w-100 h-100'  src='<?= $this->Url->build($post->image) ?>' style=''>                    
                    </div>
                    <div class='col-md-9' name='<?= $post->id ?>' id='<?= $post->id ?>'>
                        <h5><?= $post->title ?></h5>
                        <cite><small> <i class="material-icons">today</i> <?= $post->created ?></small></cite> 
                        <br><br>
                        <button class='btn btn-outline-secondary btn-sm'>Ir para o Post</button>
                        <small class='text-danger'>
                            <i class="material-icons">schedule</i> 10 min
                        </small>
                    </div>
                    
                </div>
                <hr>
            <?php endforeach; ?>  
            <div class="paginator" style="margin-top:20px">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ') ?>
                        <?= $this->Paginator->prev('< ') ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(' >') ?>
                        <?= $this->Paginator->last(' >>') ?>
                    </ul>
                </nav>
                <i class="text-secondary">
                    <small>
                        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    </small>    
                </i>
            </div>
        </div> 
        <div class='col-md-3'>
        <div class="LI-profile-badge w-200" data-width='100' data-version="v1" data-size="large" data-locale="pt_BR" data-type="vertical" data-theme="dark" data-vanity="tcdeoliveira"><a class="LI-simple-link" href='https://br.linkedin.com/in/tcdeoliveira?trk=profile-badge'>Tiago Cláudio de Oliveira</a></div>
            <hr>
            <small class='text-secondary' style='font-family: Merriweather-Light' >
            <p>Hi, how are you? I'm Glad You Came to my blog.</p>
            <p>My name is Tiago Oliveira, i'm from Brazil, i live at the sun city, Fortaleza.</p>
            <p>
                I've been working with IT since 2009 when I got my first internship as support technician at CAMED. By the end of 2009, I was hired and integrated the helpdesk team. I worked there between 2009 and 2015.
                After a year of studies, I decided to change my field and to star persueing new challenges as a programmer. So I assumed the position of PHP programmer in a project in the Assistance Office of the Military State Police of Ceará - Brazil from January to June 2016. At July 2016, I received a job offer and took on the position of SQL Developer at CHOLET, a nationally renowned and important women's clothing brand in Brazil.
                In 2018, after the end of the contract, I took up a position on the programmers team at Mobit, one of the largest and most important transit companies in Brazil.
            </p>            
            <p>
                Here we'r run an idia about PHP, SQL, .Net and other things. So, enjoy, and send me a message if you have any doubts. It's easy, just <a href='#'>click here</a>.
            </p>
            </small>        
        </div>
    </div>
</div>