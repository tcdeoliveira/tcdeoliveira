<?php $this->set('title',"Home | tcdeoliveira.com.br" ); ?>
<script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>
<div class='container'>
    <div class='row'>
        <div class='col-md-10'>
            <div class="card-deck justify-content-center offset-* mx-auto" style=''>
                <div class="card bg-dark mb-3" style="max-width: 100%;">
                    <div class="row no-gutters h-100" style='min-heigth:100%'>
                        <div class="col-md-3">
                            <img src="<?= $this->Url->build('/img/posts5ca79f5b890e6.png') ?>" class="card-img" alt="..." style='min-height:100%'>
                        </div>
                        <div class="col-md-9">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='col-md-2'>
            <div class="LI-profile-badge"  data-version="v1" data-size="medium" data-locale="pt_BR" data-type="vertical" data-theme="dark" data-vanity="tcdeoliveira"><a class="LI-simple-link" href='https://br.linkedin.com/in/tcdeoliveira?trk=profile-badge'>Tiago Cláudio de Oliveira</a></div>
        </div>
    </div>
</div>