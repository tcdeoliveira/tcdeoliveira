<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?php $this->set('title',"Edit User" ); ?>
<article class='container mh-100'>
    <div class="row users">
        <div class="col-md-12" style=''>
            <?= $this->Form->create($user) ?>
                <fieldset>
                    <legend><?= __('Edit User') ?></legend>
                    <?php
                                                            echo $this->Form->control('nome', ['class' => 'form-control']);
                                    echo $this->Form->control('email', ['class' => 'form-control']);
                                    echo $this->Form->control('password', ['class' => 'form-control']);
                                    echo $this->Form->control('rule_id', ['class' => 'form-control']);
                                    echo $this->Input->checkBox('ativo', ['value' => $user['ativo']]);
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary ']) ?>
                <?= $this->Html->link(__('List Users') , ['action' => 'index', ], ['class'=>'btn btn-dark ']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</article>