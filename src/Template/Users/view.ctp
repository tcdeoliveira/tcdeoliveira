<?php $this->set('title',"View User" ); ?>
<div class='container-fluid' style=''>
    <div class=" row">
        <div class="col-md-12"  >
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow" style='margin-bottom:15px'>
            <a class="navbar-brand" href="#">    <?= __('User Actions') ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mr-auto">
                </ul>
                <ul class="my-2 my-lg-0">     
                    <?= $this->Html->link("<i class='material-icons'>format_list_numbered</i>", ['action' => 'index'], ['escape'=>false, 'class'=>'btn btn-primary btn-sm ']) ?>
                    <?= $this->Html->link("<i class='material-icons'>format_paint</i>", ['action' => 'edit',  $user->id], ['escape'=>false,'class'=>'btn btn-warning btn-sm ']) ?>
                    <?= $this->Form->postLink("<i class='material-icons'>delete_forever</i>", ['action' => 'delete',  $user->id], ['escape'=>false,'class'=>'btn btn-danger btn-sm ','confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                </ul>
            </div>
            </nav>
        </div>
    </div>
    <div class='row'>    
    <div class="col-md-12" >        
        <h3>#<?= h($user->id) ?></h3>
        <div class="table-responsive ">
        <table class="table table-striped table-hover  table-dark shadow">
                                                                        <tr>
                            <th scope="row"><?= __('Nome') ?></th>
                            <td><?= h($user->nome) ?></td>
                        </tr>
                                                                                <tr>
                            <th scope="row"><?= __('Email') ?></th>
                            <td><?= h($user->email) ?></td>
                        </tr>
                                                                                <tr>
                            <th scope="row"><?= __('Password') ?></th>
                            <td><?= h($user->password) ?></td>
                        </tr>
                                                                                                            <tr>
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $this->Number->format($user->id) ?></td>
                    </tr>
                                    <tr>
                        <th scope="row"><?= __('Rule Id') ?></th>
                        <td><?= $this->Number->format($user->rule_id) ?></td>
                    </tr>
                                                                            <tr>
                        <th scope="row"><?= __('Created') ?></th>
                        <td><?= h($user->created) ?></td>
                    </tr>
                                    <tr>
                        <th scope="row"><?= __('Modified') ?></th>
                        <td><?= h($user->modified) ?></td>
                    </tr>
                                                                            <tr>
                        <th scope="row"><?= __('Ativo') ?></th>
                        <td><?= $user->ativo ? __('Yes') : __('No'); ?></td>
                    </tr>
                                    </table>
        </div>
            </div>    
</div>




    </div>
</div>