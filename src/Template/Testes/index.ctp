<?php $this->set('title',"Testis" ); ?>
<article class='container-fluid mh-100'>
        <div class="row" style="">
        <div class="col-md-6 offset-3">  
            <?= $this->Form->create('search', ['url' => [ 'action' => 'search']]) ?>
                <div class="input-group">            
                    <input type="text" class="form-control" placeholder="find " aria-label="find " aria-describedby="button-addon4">
                    <div class="input-group-append" id="button-addon4">
                        <button class="btn btn-primary" type="submit"><?= __('Find') ?></button>
                        <?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-dark ']) ?>
                    </div>            
                </div>
            <?= $this->Form->end() ?>
            <hr>
        </div>    
        <div class="col-md-12">      
            <div class="table-responsive">
                <table class="table table-striped table-hover  table-dark" id="data">
                    <thead class="thead-dark">
                        <tr>
                                                            <th ><?= $this->Paginator->sort('id') ?></th>
                                                            <th ><?= $this->Paginator->sort('nome') ?></th>
                                                        <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($testes as $testis): ?>
                            <tr>
                                                                                                                                                                                                                                    <td><?= $this->Number->format($testis->id) ?></td>
                                                                                                                                                                                                                                                                                                    <td><?= h($testis->nome) ?></td>
                                                                                                                                                                <td class="actions"  style="white-space:nowrap">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $testis->id], ['class'=>'btn btn-info btn-sm ']) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $testis->id], ['class'=>'btn btn-warning btn-sm ']) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $testis->id], ['class'=>'btn btn-danger btn-sm ','confirm' => __('Are you sure you want to delete # {0}?', $testis->id)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>       
        </div>
        <div class="paginator col-md-12" style="margin-top:20px">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('First')) ?>
                    <?= $this->Paginator->prev('< ' . __('Back')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Next') . ' >') ?>
                    <?= $this->Paginator->last(__('Last') . ' >>') ?>
                </ul>
            </nav>
            <i class="text-secondary">
                <small>
                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </small>    
            </i>
        </div>
    </div>
</article>