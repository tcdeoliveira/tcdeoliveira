<?php $this->set('title',"View Testis" ); ?>
<div class='container-fluid' style=''>
    <div class=" row">
        <div class="col-md-12"  >
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow" style='margin-bottom:15px'>
            <a class="navbar-brand" href="#">    <?= __('Testis Actions') ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mr-auto">
                </ul>
                <ul class="my-2 my-lg-0">     
                    <?= $this->Html->link("<i class='material-icons'>format_list_numbered</i>", ['action' => 'index'], ['escape'=>false, 'class'=>'btn btn-primary btn-sm ']) ?>
                    <?= $this->Html->link("<i class='material-icons'>format_paint</i>", ['action' => 'edit',  $testis->id], ['escape'=>false,'class'=>'btn btn-warning btn-sm ']) ?>
                    <?= $this->Form->postLink("<i class='material-icons'>delete_forever</i>", ['action' => 'delete',  $testis->id], ['escape'=>false,'class'=>'btn btn-danger btn-sm ','confirm' => __('Are you sure you want to delete # {0}?', $testis->id)]) ?>
                </ul>
            </div>
            </nav>
        </div>
    </div>
    <div class='row'>    
    <div class="col-md-12" >        
        <h3>#<?= h($testis->id) ?></h3>
        <div class="table-responsive ">
        <table class="table table-striped table-hover  table-dark shadow">
                                                                        <tr>
                            <th scope="row"><?= __('Nome') ?></th>
                            <td><?= h($testis->nome) ?></td>
                        </tr>
                                                                                                            <tr>
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $this->Number->format($testis->id) ?></td>
                    </tr>
                                                            </table>
        </div>
            </div>    
</div>




    </div>
</div>