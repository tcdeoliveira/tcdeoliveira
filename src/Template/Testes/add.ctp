<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Testis $testis
 */
?>
<?php $this->set('title',"Add Testis" ); ?>
<article class='container mh-100'>
    <div class="row testes">
        <div class="col-md-12" style=''>
            <?= $this->Form->create($testis) ?>
                <fieldset>
                    <legend><?= __('Add Testis') ?></legend>
                    <?php
                                                            echo $this->Form->control('nome', ['class' => 'form-control']);
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary ']) ?>
                <?= $this->Html->link(__('List Testes') , ['action' => 'index', ], ['class'=>'btn btn-dark ']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</article>