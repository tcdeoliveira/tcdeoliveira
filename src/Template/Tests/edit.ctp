<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Test $test
 */
?>
<?php $this->set('title',"Edit Test" ); ?>
<article class='container mh-100'>
    <div class="row tests">
        <div class="col-md-12" style='padding-bottom:5%; padding-top:5%'>
            <?= $this->Form->create($test) ?>
                <fieldset>
                    <legend><?= __('Edit Test') ?></legend>
                    <?php
                                                            echo $this->Form->control('name', ['class' => 'form-control']);
                                    
                                    echo $this->Input->checkBox('active', ['value' => $test['active']]);
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary ']) ?>
                <?= $this->Html->link(__('List Tests') , ['action' => 'index', ], ['class'=>'btn btn-dark ']) ?>
                
            <?= $this->Form->end() ?>
        </div>
    </div>
</article>