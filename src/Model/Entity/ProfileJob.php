<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProfileJob Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $empresa
 * @property string $cargo
 * @property string $ds
 * @property int $imagem
 * @property \Cake\I18n\FrozenDate $inicio
 * @property \Cake\I18n\FrozenDate|null $fim
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class ProfileJob extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'empresa' => true,
        'cargo' => true,
        'ds' => true,
        'imagem' => true,
        'inicio' => true,
        'fim' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
