<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProfileJobs Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ProfileJob get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProfileJob newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProfileJob[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProfileJob|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProfileJob|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProfileJob patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProfileJob[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProfileJob findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProfileJobsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('profile_jobs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('empresa')
            ->maxLength('empresa', 255)
            ->requirePresence('empresa', 'create')
            ->allowEmptyString('empresa', false);

        $validator
            ->scalar('cargo')
            ->maxLength('cargo', 255)
            ->requirePresence('cargo', 'create')
            ->allowEmptyString('cargo', false);

        $validator
            ->scalar('ds')
            ->requirePresence('ds', 'create')
            ->allowEmptyString('ds', false);

        $validator
            ->integer('imagem')
            ->requirePresence('imagem', 'create')
            ->allowEmptyFile('imagem', false);

        $validator
            ->date('inicio')
            ->requirePresence('inicio', 'create')
            ->allowEmptyDate('inicio', false);

        $validator
            ->date('fim')
            ->allowEmptyDate('fim');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
