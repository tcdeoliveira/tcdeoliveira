<?php
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('users');
        $this->setDisplayField('nome');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Rules', [
            'foreignKey' => 'rule_id',
            'joinType' => 'INNER'
        ]);
    }
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');
        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome', 'Este campo deve ser preenchido')
            ->allowEmptyString('nome', false);           
        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email', 'Este campo deve ser preenchido')
            ->allowEmptyString('email', false)
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);
        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->allowEmptyString('password', false)
            ->notEmpty('password', 'Este campo deve ser preenchido')
            ->notEmpty('confirm_password', 'Este campo deve ser preenchido')
            ->regex('confirm_password','#^(?=.*?[A-z])(?=.*?[0-9]).{8,}$#','* A senha deve conter no mínimo 8 caracteres incluindo letras e números. ')                
            ->regex('confirm_password','#^(?=.*?[A-z])(?=.*?[0-9]).{8,}$#','* A senha deve conter no mínimo 8 caracteres incluindo letras e números. ')               
            ->sameas('password','confirm_password','* As senhas não conferem.')
            ->sameas('confirm_password','password','* As senhas não conferem.');  
        $validator
            ->boolean('ativo')
            ->requirePresence('ativo', 'create')
            ->allowEmptyString('ativo', false);
        return $validator;
    }
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['rule_id'], 'Rules'));
        return $rules;
    }
}