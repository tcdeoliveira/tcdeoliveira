<?php
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
Router::defaultRouteClass(DashedRoute::class);
Router::scope('/', function (RouteBuilder $routes) {
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));
    $routes->applyMiddleware('csrf');
    $routes->connect('/', ['controller' => 'Posts', 'action' => 'index']);
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);
    $routes->connect('/contato', ['controller' => 'Pages', 'action' => 'display', 'contato']);
    $routes->connect('/admin/regras', ['controller' => 'Rules', 'action' => 'index', 'prefix'=>'admin']);
    $routes->connect('/admin/regras/add', ['controller' => 'Rules', 'action' => 'add', 'prefix'=>'admin']);
    $routes->connect('/admin/regras/editar/*', ['controller' => 'Rules', 'action' => 'edit', 'prefix'=>'admin']);
    $routes->connect('/admin/usuarios', ['controller' => 'Users', 'action' => 'index', 'prefix'=>'admin']);
    $routes->connect('/admin/usuarios/novo', ['controller' => 'Users', 'action' => 'add', 'prefix'=>'admin']);
    $routes->connect('/admin/usuarios/editar/*', ['controller' => 'Users', 'action' => 'edit', 'prefix'=>'admin']);
    $routes->connect('/admin/categorias', ['controller' => 'Categories', 'action' => 'index', 'prefix'=>'admin']);
    $routes->connect('/admin/categorias/novo', ['controller' => 'Categories', 'action' => 'add', 'prefix'=>'admin']);
    $routes->connect('/admin/categorias/editar/*', ['controller' => 'Categories', 'action' => 'edit', 'prefix'=>'admin']);
    $routes->fallbacks(DashedRoute::class);
});       
Router::prefix('admin', function ($routes) {
    $routes->connect('/', ['controller' => 'Users', 'action' => 'index']);
    $routes->fallbacks('DashedRoute'); 
});