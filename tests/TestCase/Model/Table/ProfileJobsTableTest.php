<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProfileJobsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProfileJobsTable Test Case
 */
class ProfileJobsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProfileJobsTable
     */
    public $ProfileJobs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ProfileJobs',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProfileJobs') ? [] : ['className' => ProfileJobsTable::class];
        $this->ProfileJobs = TableRegistry::getTableLocator()->get('ProfileJobs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProfileJobs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
