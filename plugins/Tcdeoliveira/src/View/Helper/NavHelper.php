<?php 
namespace Tcdeoliveira\View\Helper;
use Cake\View\Helper;
use Cake\View\Helper\UrlHelper;
use Cake\View\Form\ContextFactory;
use Cake\View\Form\ContextInterface;
Class NavHelper extends Helper {
    
    public function sideBar($user){
        $sidebar = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'sidebar.ctp';
        if (file_exists($sidebar)) 
        {
            ob_start();
            include $sidebar;
            echo ob_get_clean();
        }
        else 
        {
            echo $this->element('sidebar');
        }
    }
    public function topBar(){
        $top = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'top.ctp';
        if (file_exists($top)) {
            ob_start();
            include $top;
            echo ob_get_clean();
        }
        else {
            echo $this->element('top');
        }
    }
}