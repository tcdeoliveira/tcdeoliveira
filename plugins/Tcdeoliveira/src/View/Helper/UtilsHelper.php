<?php 
    namespace Tcdeoliveira\View\Helper;
    use Cake\View\Helper;
    use Cake\View\Helper\UrlHelper;
    use Cake\View\Form\ContextFactory;
    use Cake\View\Form\ContextInterface;
    Class UtilsHelper extends Helper {
        public function avaliation($avaliation){            
            switch ($avaliation){
                case 0:
                    $var = '<span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span>';
                break;
                case ($avaliation > 0 ) &&  ($avaliation < 1 ) :
                    $var = '<span class="icon-star-half text-warning"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span>';
                break;
                case ($avaliation >= 1 ) &&  ($avaliation < 2 ) :
                    $var = '<span class="icon-star-full text-warning"></span>';
                    if(strlen($avaliation)>1){
                        $var = $var.'<span class="icon-star-half text-warning"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span>';
                    }
                    else {
                        $var = $var.'<span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span>';
                    }
                break;
                case ($avaliation >= 2 ) &&  ($avaliation < 3 ) :
                    $var = '<span class="icon-star-full text-warning"></span><span class="icon-star-full text-warning"></span>';
                    if(strlen($avaliation)>1){
                        $var = $var.'<span class="icon-star-half text-warning"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span>';
                    }
                    else {
                       $var = $var.'<span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span>'; 
                    }
                break;
                case ($avaliation >= 3 ) &&  ($avaliation < 4 ) :
                    $var = '<span class="icon-star-full text-warning"></span><span class="icon-star-full text-warning"></span><span class="icon-star-full text-warning"></span>';
                    if(strlen($avaliation)>1){
                        $var = $var.'<span class="icon-star-half text-warning"></span><span class="icon-star-full text-secondary"></span>';
                    }
                    else {$var = $var.'<span class="icon-star-full text-secondary"></span><span class="icon-star-full text-secondary"></span>';}
                break;
                case ($avaliation >= 4 ) &&  ($avaliation < 5 ) :
                    $var = '<span class="icon-star-full text-warning"></span><span class="icon-star-full text-warning"></span><span class="icon-star-full text-warning"></span><span class="icon-star-full text-warning"></span>';
                    if(strlen($avaliation)>1){
                        $var = $var.'<span class="icon-star-half text-warning"></span>';
                    }
                    else{$var = $var.'<span class="icon-star-full text-secondary"></span>';}
                break;
                case 5  :
                    $var = '<span class="icon-star-full text-warning"></span><span class="icon-star-full text-warning"></span><span class="icon-star-full text-warning"></span><span class="icon-star-full text-warning"></span><span class="icon-star-full text-warning"></span>';                    
                break;              
                

            }
            return $var;
        }
    }
?>