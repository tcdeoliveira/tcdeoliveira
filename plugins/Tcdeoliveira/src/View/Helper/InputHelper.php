<?php 
namespace Tcdeoliveira\View\Helper;
use Cake\View\Helper;
use Cake\View\Helper\UrlHelper;
use Cake\View\Form\ContextFactory;
use Cake\View\Form\ContextInterface;
Class InputHelper extends Helper {
    public function makeDate($format, $date){
        return date($format,  strtotime(str_replace('/', '-', $date)));
    }    
    public function select($name, $array, $options = false){
        $required_label = null;
        $required_input = null;
        if(isset($options['onclick'])):
            $onclick = "onclick='".$options['onclick']."()'" ;
        endif;
        if(isset($options['label'])):
            $label = $options['label'];        
            else:
                $label = $name; 
            endif;
        if(isset($options['icon'])):
            $icon = $options['icon'];
            else:
                $icon = "Não está na lista";
        endif;        
        if(isset($options['required'])&&($options['required'])){
            $required_label = "<span class='text-warning label-required'> *</span>";
            $required_input = "required";
        }
        foreach ($array as $a):
            if($a['id']==$options['value']):
                $option = $option.'<option selected value="'.$a['id'].'">'.$a['name'].'</option>';    
                else:
                    $option = $option.'<option value="'.$a['id'].'">'.$a['name'].'</option>';
            endif;            
        endforeach;
        
            $botao = "            
                <div class='form-group '>
                    <label for='$name'>$label $required_label</label>
                    <div class='input-group' >
                        <div class='input-group-prepend'>
                            <button class='btn btn-primary' type='button' $onclick>$icon</button>
                        </div>    
                        <select  multiple  class='custom-select' name='$name' id='$name' $required_input>
                                      
                            $option                
                        </select>                
                    </div>
                </div>
                "
                ; 
        
        echo $botao;
    }
    public function date($name, $value, $options = false ) {
        $required_label = null;
        $required_input = null;
        $label = $name;        
        $checked = null;        
        if(isset($options['label'])){
            $label = $options['label'];
        }
        if( !is_null($value) ){
            debug($value);
            $value = $this->makeDate("Y-m-d", $value);
        }
        if(!$label){
            $label = "$name";
        }   
        if(isset($options['required'])&&($options['required'])){
            $required_label = "<span class='text-warning label-required'> *</span>";
            $required_input = "required";
        }
        echo "
        <div class='form-group'>
            <div class='form-group' >
                <label class='' for='$name'>$label $required_label</label>
                <input type='date' class='form-control' value='$value' $required_input name='$name' id='$name' $checked >                
            </div>
        </div>
        ";  
    }
    public function checkBox($name, $options = false ) {
        $required_label = null;
        $required_input = null;
        $label = $name;        
        $checked = null;    
        $t = 1;
        $f = 0;
        $options['required']=false;
        if(isset($options['label'])){
            $label = $options['label'];
        }
        if(isset($options['value'])&&($options['value'])){
            $checked = "checked";
        }
        if(!$label){
            $label = "$name";
        }   
        if($options['required']){
            $required_label = "<span class='text-warning label-required'> *</span>";
            $required_input = "required";
        }

        
        echo "
        <div class='form-group'>
            <div class='custom-control custom-checkbox' >
                <input type='hidden' name='$name' value='$f' />
                <input type='checkbox' class='custom-control-input' value='$t' $required_input name='$name' id='$name' $checked >
                <label class='custom-control-label' for='$name'>$label $required_label</label>
                <br>
            </div>
        </div>
        ";          
    }
    public function file($name, $options = false){     
        $errors = null;  
        $required = null;
        $required_label = null;   
        $readonly = (isset($options['readonly']) && $options['readonly']) ? 'readonly' : '';
        $value = isset($options['value']) ? $options['value'] : null;
        $label = isset($options['label']) ?  ucfirst( $options['label'] ) :  ucfirst($name);
        $placeholder = isset($options['placeholder']) ? $options['placeholder'] : '';
        if(isset($options['required']) && $options['required']){
            $required = 'required';
            $required_label = "<span><b class='text-danger'>*</b></span>";
        }
        if(!empty($options['Errors'])){
            foreach($options['Errors'] as $erro){$errors .= "<div class='invalid-feedback'>".$erro."</div>";}
            return "
            <label for='$name'>$required_label $label</label>                    
            <div class='input-group mb-3 was-validated'>
                <input $required type='file' value='$value' title='$placeholder' name='$name'  class='custom-file-input' id='$name' required>
                <label class='custom-file-label' for='$name'></label>$placeholder</label>$value
                $errors
            </div>             
            ";
        }
        else{
            return "
            <label for='$name'>$required_label $label</label>                
            <div class='input-group mb-3'>            
                <div class='custom-file'>
                    <input $required type='file' value='$value' title='$placeholder' name='$name' class='custom-file-input' id='inputGroupFile01' aria-describedby='inputGroupFileAddon01'>
                    <label class='custom-file-label' for='$name'>$placeholder</label> $value                   
                </div>
            </div> 
            ";
        }
    }
    public function miniatura($imagem){
        $size =  strlen($imagem);
        $first_bar = strripos ($imagem, DS);
        $file_name = substr(strrchr($first_bar, 10), 1 );
        echo $file_name;
        $VAR = substr($imagem, 0, ($first_bar-$size+1)).'thumbnails'.DS.'thumb_'.substr($imagem, $first_bar+1);        
        return $VAR;
    }
    public function text($name, $options = null){
        $errors = null;  
        $required = null;
        $required_label = null;   
        $readonly = (isset($options['readonly']) && $options['readonly']) ? 'readonly' : '';
        $value = isset($options['value']) ? $options['value'] : null;
        $label = isset($options['label']) ?  ucfirst( $options['label'] ) :  ucfirst($name);
        $placeholder = isset($options['placeholder']) ? $options['placeholder'] : '';
        if(isset($options['required']) && $options['required']){
            $required = 'required';
            $required_label = "<span><b class='text-danger'>*</b></span>";
        }
        if(!empty($options['Errors'])){
            foreach($options['Errors'] as $erro){$errors .= "<div class='text-danger'><small>".$erro."</small></div>";}
            return "
            <div class='form-group'>
                <label for='$name'>$required_label $label</label>
                <input type='text' class='form-control is-invalid' value='$value' $readonly id='$name' name='$name' $required  placeholder='$placeholder'>
                $errors
            </div>
            ";
        }
        return "
        <div class='form-group'>
            <label for='$name'>$required_label $label</label>
            <input type='text' class='form-control'  value='$value' $readonly id='$name' name='$name' $required  placeholder='$placeholder'>
        </div>
        ";     
    }
    public function textarea($name, $options = null){
        $errors = null;  
        $required = null;
        $required_label = null;   
        $readonly = (isset($options['readonly']) && $options['readonly']) ? 'readonly' : '';
        $value = isset($options['value']) ? $options['value'] : null;
        $label = isset($options['label']) ?  ucfirst( $options['label'] ) :  ucfirst($name);
        $placeholder = isset($options['placeholder']) ? $options['placeholder'] : '';
        if(isset($options['required']) && $options['required']){
            $required = 'required';
            $required_label = "<span><b class='text-danger'>*</b></span>";
        }
        if(!empty($options['Errors'])){
            foreach($options['Errors'] as $erro){$errors .= "<div class='text-danger'><small>".$erro."</small></div>";}
        }
        return 
        "
        <div class='form-group'>
            <label for='$name'>$required_label $label</label>
            <textarea name='$name' id='$name' $readonly  $required  class='form-control' >$value</textarea>
            <script>
                CKEDITOR.replace( '$name' );
            </script>
            $errors
        </div>
        ";
    }
}