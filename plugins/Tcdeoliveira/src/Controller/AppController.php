<?php
namespace Tcdeoliveira\Controller;
use App\Controller\AppController as BaseController;
class AppController extends BaseController
{
    /*
        public $paginate = [
        'limit' => 25
        ];
        
    */
    public $helpers = [
        'Paginator' => ['templates' => 'Tcdeoliveira.paginator-templates'],
        'Form' => ['templates' => 'Tcdeoliveira.form-templates']
    ];
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->viewBuilder()->setTheme('Tcdeoliveira');
    }
}