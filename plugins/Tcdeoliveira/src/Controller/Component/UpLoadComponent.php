<?php 
    namespace Tcdeoliveira\Controller\Component;    
    use Cake\Controller\Component;
    use Cake\Filesystem\Folder;
    use Cake\Filesystem\File;    
    class UpLoadComponent extends Component {
        //Componente Flash
        public $components = ['Flash'];
        public function createThumbnails($original, $tofolder, $name){    
            if(!is_dir(WWW_ROOT.$tofolder.'thumbnails')){
                $dir = new Folder();
                $dir->create(WWW_ROOT.$tofolder.DS.'thumbnails');                    
            }        
            list($width, $height) = getimagesize($original);        
            switch ($width){
                case ($width > $height):
                    $nh = 220; $nw = 250;
                    break;
                case ($width < $height):
                    $nh = 250; $nw = 220;
                    break;
                ;
                default:
                $nh = 220; $nw = 250;
            }            
            $image_p = imagecreatetruecolor($nw, $nh);
            $image = imagecreatefromjpeg($original);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $nw, $nh, $width, $height);           
            imagejpeg($image_p, WWW_ROOT.$tofolder.DS.'thumbnails'.DS.'thumb_'.$name, 80);
            imagedestroy($image_p);
        }
        //Valida o arquivo enviado
        public function check($allowed, $file_name){
            if((in_array(strtoupper(pathinfo($file_name, PATHINFO_EXTENSION)), $allowed)) || (in_array(strtolower(pathinfo($file_name, PATHINFO_EXTENSION)), $allowed))):
                return pathinfo($file_name, PATHINFO_EXTENSION);            
            endif;
            return false;
        }
        public function isImage($file_name){
           if( !in_array(strtoupper(pathinfo($file_name, PATHINFO_EXTENSION)), ['JPG', 'JPEG', 'PNG', 'GIF', 'BMP', 'JFIF', 'TIFF']) )
           {
               return false;
           }
           return true;
        }
        //Envia o arquivo para o servidor


        public function send($file, $folder){
            if(!is_dir(WWW_ROOT.$folder)){
                $_folder = new Folder();
                $_folder->create(WWW_ROOT.$folder);
            }
            $_file = new File($file['tmp_name']);
            $_file->name = uniqid().'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if($_file->copy(WWW_ROOT.$folder.$_file->name)){
                return $folder.DS.$_file->name;
            }
            return false;
        }

        public function send_old($expected, $file, $tofolder){  
            if($ext=$this->check($expected, $file['name'], $tofolder)){
                if(!is_dir(WWW_ROOT.$tofolder)){                
                    $dir = new Folder();
                    if(!$dir->create(WWW_ROOT.$tofolder)){
                        $this->Flash->error(__('Pasta de destino não existe e não foi possível cria-la.'));
                        return false;
                    }      
                }
                if($newfile = new File($file['tmp_name'])){
                    if($newfile->name = uniqid().".".$ext)
                    {
                        $newfile->copy(WWW_ROOT.$tofolder.DS.$newfile->name);
                        if($this->isImage($newfile->name)){
                            $this->createThumbnails(WWW_ROOT.$tofolder.$newfile->name, $tofolder, $newfile->name);
                        }
                        return  $newfile->name;                       
                    }
                    else {
                        $this->Flash->error(__('Falha ao renomear o arquivo.'), ['escape'=>false]);
                        return false; 
                    }
                }
                else {
                    $this->Flash->error(__('Falha carregar o arquivo temporário.'), ['escape'=>false]);
                    return false; 
                }
            }
            else { 
                $this->Flash->error(__('A extenção <b>'.pathinfo($file['name'], PATHINFO_EXTENSION).'</b> não é permitida para está ação.'), ['escape'=>false]);
                return false;        
            }
        }
        public function deleteFile($file){
            $file = new File($file);
            debug($file);
            echo "to aqui";
            if($file->delete()){
                return 1;
            }
            else { return 0 ;}
        }
        public function deleteMini($imagem){
            $size =  strlen($imagem);
            $first_bar = strripos ($imagem, DS);
            $file_name = substr(strrchr($first_bar, 10), 1 );
            echo $file_name;
            $VAR = substr($imagem, 0, ($first_bar-$size+1)).'thumbnails'.DS.'thumb_'.substr($imagem, $first_bar+1);        
            $file = new File(WWW_ROOT.$VAR);
            return $file->delete();            
        }
        
    }