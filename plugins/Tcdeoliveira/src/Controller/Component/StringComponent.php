<?php 
    namespace Tcdeoliveira\Controller\Component;    
    use Cake\Controller\Component;
    class StringComponent extends Component {
        public function removerAcentos($string){            
            $from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
            $to = "aaaaeeiooouucAAAAEEIOOOUUC";        
            $keys = array();
            $values = array();
            preg_match_all('/./u', $from, $keys);
            preg_match_all('/./u', $to, $values);
            $mapping = array_combine($keys[0], $values[0]);
            return strtr($string, $mapping);
        }
        public function removerPontuacao($string){            
            $pontuacao = [
                ',','%','.',';',':','?','!','|','(',')','{','}','[',']','/','\\'
            ];
            return str_replace($pontuacao, "", $string);
        }
        public function generateUrl($string){
            $url = strtolower(str_replace(' ', '-', $this->removerPontuacao($this->removerAcentos($string)))); 
            return $url;
        }
    }