<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo  $title; ?></title>
        <!-- Bootstrap CSS CDN -->
        <?= $this->Html->meta('icon') ?>
        <?= $this->Html->css('bootstrap.css') ?>  
        <?= $this->Html->css('icomon.css') ?>
        <?= $this->Html->css('material-icons.css') ?> 
        <?= $this->Html->css('animate.css') ?> 
        <?= $this->Html->css('tcdeoliveira.css') ?> 
        <?= $this->Html->script('ckeditor/ckeditor.js') ?>
        <!-- Modifie CSS proprietes here -->
        <?= $this->Html->css('app.css') ?>        
        <?= $this->fetch('script') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>    
    </head> 
    <body cz-shortcut-listen="true" class="bg-dark text-secondary">
        <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm">
            <div class='logo shadow-sm'><b>TC</b></div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto my-2 my-lg-0">                    
                </ul>
                <ul class="navbar-nav  my-2 my-lg-0">
                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            AdminTools
                        </a>
                        <div class="dropdown-menu shadow-lg thead-dark" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= $this->Url->build('/admin/categorias') ?>">Categorias</a>
                        <a class="dropdown-item" href="<?= $this->Url->build('/admin/posts') ?>">Posts</a>
                        <a class="dropdown-item" href="<?= $this->Url->build('/admin/regras') ?>">Regras de Acesso</a>
                        <a class="dropdown-item" href="<?= $this->Url->build('/admin/usuarios') ?>">Usuários</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $this->Url->build('/') ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $this->Url->build('/quem-sou') ?>">Quem Sou</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $this->Url->build('/contato') ?>">Contato</a>
                    </li>
                </ul>               
            </div>
        </nav>
        </header>
        <section class='bg-light' style='padding-top:5%; padding-bottom:5%' >
            <article class='container mh-100'>
                <?= $this->Flash->render() ?>
            </article>
            <?= $this->fetch('content') ?>
        </section>
        <footer class='text-center text-white'>
            <div style="opacity:0.5">
                <h6>tcdeoliveira.com.br</h6>               
            </div>
            <hr>
        </footer>
    </body>
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <?= $this->Html->script('jquery-3.3.1.slim.min.js') ?>
    <?= $this->Html->script('popper.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('docs.min.js') ?>
    <?= $this->Html->script('docsearch.min.js') ?>    
</html>