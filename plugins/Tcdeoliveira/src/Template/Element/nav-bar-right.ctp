<ul class="navbar-nav my-2 my-lg-0">
    <?php 
    	//Variavel que escreve o menu do usuáro
        if(isset($logado)):
            echo "
            <li class=\"nav-item\"><a class=\"nav-link text-warning\" href=\"".$this->Url->build('/minha-conta')."\">" . $logado['login'] . " <span class=\"icon-pacman\"></span></a></li>".
            "<li class=\"nav-item\"><a class=\"nav-link text-danger\" href=\"".$this->Url->build('/logout')."\"> <i class=\"material-icons icon-user text-danger\">exit_to_app</i>Fazer logoff</a></li>";
            
            else:
                echo "
                <li class=\"nav-item\"><a class=\"nav-link\" href=\"".$this->Url->build('/login')."\"><i class=\"material-icons  icon-user text-secondary\">account_circle</i></a></li>";
        endif;
    ?>

</ul>