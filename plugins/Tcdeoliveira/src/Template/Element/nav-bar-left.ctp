<ul class="navbar-nav mr-auto">        
    <li class="nav-item active">
        <a class="nav-link" href="<?= $this->url->build('/funcionarios') ?>">Funcionários</a>        
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="<?= $this->url->build('/livros') ?>">Livros</a>        
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="<?= $this->url->build('/setores') ?>">Setores</a>        
    </li>
</ul>