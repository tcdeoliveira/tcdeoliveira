$(document).ready(function(){
    $('.data').mask('00/00/0000', {placeholder: "xx/xx/xxxx"});
    $('.tempo').mask('00:00:00', {placeholder: "xx:xx:xx"});
    $('.hora').mask('00:00', {placeholder: "hh:mm"});
    $('.data_tempo').mask('00/00/0000 00:00:00');
    $('.cep').mask('00000-000', {placeholder: "00000-000"});
    $('.tel').mask('00000-0000');
    $('.fixo').mask('(00)0000-0000', {placeholder: "(xx)xxxx-xxxx"});
    $('.celular').mask('(00)00000-0000', {placeholder: "(xx)xxxxx-xxxx"});
    $('.telefone').mask('(00)0000-00009', {placeholder: "(00)0000-00000"});
    $('.ddd_tel').mask('(00) 0000-0000');
    $('.cpf').mask('000.000.000-00');
    $('#cnpj').mask('00.000.000/0000-00', {placeholder: "xx.xxx.xxx/xxxx-xx"});
    $('.dinheiro').mask('000.000.000.000.000,00' , { reverse : true});
    $('.dinheiro2').mask("#.##0,00" , { reverse:true});
    $('.cor_hex').mask('#xxxxxx' , {
            translation: {
            'x': {
                    pattern: /[a-fA-F0-9]/
            },
            '#' : ''
        }
    });			
    $('.placeholder').mask("00/00", { placeholder: "xx/xx" }) ;
    $('#unmask').click(function(){
            var unmask_value = $('.cpf').cleanVal();
            $('#clearcpf').html(unmask_value);
    });
});