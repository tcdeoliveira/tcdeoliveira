$(function(){
    $('.noSpacesField').bind('input', function(){
      $(this).val(function(_, v){
       return v.replace(/\s+/g, '');
      });
    });
  });