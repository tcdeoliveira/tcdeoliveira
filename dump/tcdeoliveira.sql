-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 23-Abr-2019 às 19:39
-- Versão do servidor: 5.7.24
-- versão do PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tcdeoliveira`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id`, `name`, `active`) VALUES
(1, 'PHP', 1),
(2, 'PPO - Programação Orientada a Objetos', 1),
(3, 'C#', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `phinxlog`
--

DROP TABLE IF EXISTS `phinxlog`;
CREATE TABLE IF NOT EXISTS `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20190307172835, 'CreateUsers', '2019-03-07 20:28:45', '2019-03-07 20:28:45', 0),
(20190311141015, 'CreateTests', '2019-03-11 17:10:36', '2019-03-11 17:10:37', 0),
(20190312160041, 'CreateTestes', '2019-03-12 19:00:50', '2019-03-12 19:00:51', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `active` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `title`, `url`, `image`, `text`, `active`, `user_id`, `created`, `modified`) VALUES
(7, 1, 'Encontrando valores em um array', 'encontrando-valores-em-um-array', '\\img\\posts\\5cb8b083a0c59.jpg', '<p>Um array &eacute; um elemento comumente utilizado na programa&ccedil;&atilde;o, tamb&eacute;m &eacute; comum precisarmos de um valor que est&aacute; armazenado dentro de determinado array, entretanto como sabermos se o valor est&aacute; mesmo la dentro sem &quot;percorer&quot; todos os items e compararmos, com um foreach por exemplo?</p>\r\n\r\n<p>Cl&aacute;ro, isso poderia ser resolvido com um foreach, mas o PHP nos fornecesse uma fun&ccedil;&atilde;o nativa, simples, que pode facilitar muito pro nosso lado, eu falo da fun&ccedil;&atilde;o &quot;array_search&quot;.</p>\r\n\r\n<p>B&aacute;sicamente,&nbsp;Pesta fun&ccedil;&atilde;o ir&aacute; procura por um valor em um array e retorna sua chave correspondente caso seja encontrado.</p>\r\n\r\n<p>A sintaxe &eacute; bem simples:</p>\r\n\r\n<p>array_search&nbsp;(&nbsp;<a href=\"https://www.php.net/manual/pt_BR/language.pseudo-types.php#language.types.mixed\">mixed</a>&nbsp;<code>$needle</code>&nbsp;,&nbsp;array&nbsp;<code>$haystack</code>&nbsp;[,&nbsp;bool&nbsp;<code>$strict</code>&nbsp;= false&nbsp;] ) :&nbsp;<a href=\"https://www.php.net/manual/pt_BR/language.pseudo-types.php#language.types.mixed\">mixed</a></p>\r\n\r\n<p>Procura em&nbsp;haystack&nbsp;por&nbsp;needle.</p>\r\n\r\n<p>Vale lembrar:</p>\r\n\r\n<p>Esta fun&ccedil;&atilde;o pode retornar o booleano&nbsp;<code>FALSE</code>, mas tamb&eacute;m pode retornar um valor n&atilde;o-booleano que pode ser avaliado como&nbsp;<code>FALSE</code>, como&nbsp;<em>0</em>&nbsp;ou &quot;&quot;. Leia a se&ccedil;&atilde;o em&nbsp;<a href=\"https://www.php.net/manual/pt_BR/language.types.boolean.php\">Booleanos</a>&nbsp;para maiores informa&ccedil;&otilde;es. Utilize o&nbsp;<a href=\"https://www.php.net/manual/pt_BR/language.operators.comparison.php\">operador ===&nbsp;</a>para testar o valor retornado por esta fun&ccedil;&atilde;o.</p>\r\n\r\n<p>Voc&ecirc; pode ver a documenta&ccedil;&atilde;o completa neste <a href=\"https://www.php.net/manual/pt_BR/function.array-search.php\">link</a>.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 2, '2019-04-18 17:14:43', '2019-04-23 17:52:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `profile_jobs`
--

DROP TABLE IF EXISTS `profile_jobs`;
CREATE TABLE IF NOT EXISTS `profile_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `empresa` varchar(255) NOT NULL,
  `cargo` varchar(255) NOT NULL,
  `ds` text NOT NULL,
  `imagem` int(11) NOT NULL,
  `inicio` date NOT NULL,
  `fim` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `rules`
--

DROP TABLE IF EXISTS `rules`;
CREATE TABLE IF NOT EXISTS `rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `rules`
--

INSERT INTO `rules` (`id`, `name`) VALUES
(1, 'Administrador'),
(2, 'Escritor'),
(3, 'Super Usuário');

-- --------------------------------------------------------

--
-- Estrutura da tabela `testes`
--

DROP TABLE IF EXISTS `testes`;
CREATE TABLE IF NOT EXISTS `testes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `testes`
--

INSERT INTO `testes` (`id`, `nome`) VALUES
(1, 'sfsdfsd');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tests`
--

DROP TABLE IF EXISTS `tests`;
CREATE TABLE IF NOT EXISTS `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tests`
--

INSERT INTO `tests` (`id`, `name`, `active`) VALUES
(1, 'Tiago', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `EMAIL_UNIQUE_INDEX` (`email`),
  KEY `rule_id` (`rule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `nome`, `email`, `password`, `rule_id`, `ativo`, `created`, `modified`) VALUES
(2, 'Tiago Oliveira', 'tcdeoliveira@outlook.com', '$2y$10$Fkub26gHA8RlahcOL4PNI./Iu57wjVii84GTKkxk5BEECmSK6GmxC', 1, 1, '2019-04-02 19:13:14', '2019-04-02 19:13:14');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `profile_jobs`
--
ALTER TABLE `profile_jobs`
  ADD CONSTRAINT `profile_jobs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `rules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
